package characters;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;


import game.Main;

import game.Refresh;
import objects.GameObject;
import objects.Piece;
import utils.Res;
import utils.Utils;


public class Mario extends BasicCharacter {

    public static final int MARIO_OFFSET_Y_INITIAL = 243;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int WIDTH = 28;
    public static final int HEIGHT = 50;
    public static final int JUMPING_LIMIT = 42;

    private Image imgMario;
    private boolean jumping;
    private int jumpingExtent;

    public static int countPiece = 0;


    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String motionDirectionMario;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;

            motionDirectionMario = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            motionDirectionMario = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            motionDirectionMario = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(motionDirectionMario);
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            Main.scene.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            Main.scene.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                Main.scene.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                Main.scene.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece)
                || this.hitBelow(piece)
                ) {

            countPiece += 10;
            Main.labelScore.setText(Integer.toString(countPiece));
            System.out.println(countPiece);
            return true;
        }


        return false;
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) || this.hitBack(pers)) {
            if (pers.alive) {
                this.setMoving(false);
                if (countPiece == 0) {
                    this.setAlive(false);//false mario morto
                    Main.labelScore.setVisible(false);
                    Main.labelGameOver.setVisible(true);
                    Main.buttonNewGame.setVisible(true);
                    Main.buttonCloseGame.setVisible(true);

                    Main.buttonNewGame.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            new Refresh().replayGame();
                        }
                    });

                    Main.buttonCloseGame.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Main.windowGame.dispatchEvent(new WindowEvent(Main.windowGame, WindowEvent.WINDOW_CLOSING));
                        }
                    });

                }
            } else this.alive = true;
        } else if (this.hitBelow(pers)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
    }
}
