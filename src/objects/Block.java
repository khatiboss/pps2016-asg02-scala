package objects;

import utils.Res;
import utils.Utils;

public class Block extends GameObject {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(Res.IMG_BLOCK);
    }

}
