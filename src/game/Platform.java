package game;

import objects.Block;
import characters.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import objects.GameObject;
import objects.Piece;
import objects.Tunnel;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    public static final int MARIO_FREQUENCY = 25;
    public static final int MUSHROOM_FREQUENCY = 45;
    public static final int TURTLE_FREQUENCY = 45;
    public static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    public static final int TURTLE_DEAD_OFFSET_Y = 30;
    public static final int FLAG_X_POS = 4650;
    public static final int CASTLE_X_POS = 4850;
    public static final int FLAG_Y_POS = 115;
    public static final int CASTLE_Y_POS = 145;
    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    public Mario mario;
    public Mushroom mushroom;
    public Turtle turtle;

    public Tunnel tunnel1;
    public Tunnel tunnel2;
    public Tunnel tunnel3;
    public Tunnel tunnel4;
    public Tunnel tunnel5;
    public Tunnel tunnel6;
    public Tunnel tunnel7;
    public Tunnel tunnel8;

    public Block block1;
    public Block block2;
    public Block block3;
    public Block block4;
    public Block block5;
    public Block block6;
    public Block block7;
    public Block block8;
    public Block block9;
    public Block block10;
    public Block block11;
    public Block block12;

    public Piece piece1;
    public Piece piece2;
    public Piece piece3;
    public Piece piece4;
    public Piece piece5;
    public Piece piece6;
    public Piece piece7;
    public Piece piece8;
    public Piece piece9;
    public Piece piece10;

    private Image imgFlag;
    private Image imgCastle;

    private ArrayList<GameObject> objects;
    private ArrayList<Piece> pieces;

    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        mario = new Mario(300, 245);
        mushroom = new Mushroom(800, 263);
        turtle = new Turtle(950, 243);

        tunnel1 = new Tunnel(600, 230);
        tunnel2 = new Tunnel(1000, 230);
        tunnel3 = new Tunnel(1600, 230);
        tunnel4 = new Tunnel(1900, 230);
        tunnel5 = new Tunnel(2500, 230);
        tunnel6 = new Tunnel(3000, 230);
        tunnel7 = new Tunnel(3800, 230);
        tunnel8 = new Tunnel(4500, 230);

        block1 = new Block(400, 180);
        block2 = new Block(1200, 180);
        block3 = new Block(1270, 170);
        block4 = new Block(1340, 160);
        block5 = new Block(2000, 180);
        block6 = new Block(2600, 160);
        block7 = new Block(2650, 180);
        block8 = new Block(3500, 160);
        block9 = new Block(3550, 140);
        block10 = new Block(4000, 170);
        block11 = new Block(4200, 200);
        block12 = new Block(4300, 210);

        piece1 = new Piece(402, 145);
        piece2 = new Piece(1202, 140);
        piece3 = new Piece(1272, 95);
        piece4 = new Piece(1342, 40);
        piece5 = new Piece(1650, 145);
        piece6 = new Piece(2650, 145);
        piece7 = new Piece(3000, 135);
        piece8 = new Piece(3400, 125);
        piece9 = new Piece(4200, 145);
        piece10 = new Piece(4600, 40);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        objects = new ArrayList<GameObject>();

        this.objects.add(tunnel1);
        this.objects.add(tunnel2);
        this.objects.add(tunnel3);
        this.objects.add(tunnel4);
        this.objects.add(tunnel5);
        this.objects.add(tunnel6);
        this.objects.add(tunnel7);
        this.objects.add(tunnel8);

        this.objects.add(block1);
        this.objects.add(block2);
        this.objects.add(block3);
        this.objects.add(block4);
        this.objects.add(block5);
        this.objects.add(block6);
        this.objects.add(block7);
        this.objects.add(block8);
        this.objects.add(block9);
        this.objects.add(block10);
        this.objects.add(block11);
        this.objects.add(block12);

        pieces = new ArrayList<Piece>();
        this.pieces.add(this.piece1);
        this.pieces.add(this.piece2);
        this.pieces.add(this.piece3);
        this.pieces.add(this.piece4);
        this.pieces.add(this.piece5);
        this.pieces.add(this.piece6);
        this.pieces.add(this.piece7);
        this.pieces.add(this.piece8);
        this.pieces.add(this.piece9);
        this.pieces.add(this.piece10);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (int i = 0; i < objects.size(); i++) {
            if (this.mario.isNearby(this.objects.get(i)))
                this.mario.contact(this.objects.get(i));

            if (this.mushroom.isNearby(this.objects.get(i)))
                this.mushroom.contact(this.objects.get(i));

            if (this.turtle.isNearby(this.objects.get(i)))
                this.turtle.contact(this.objects.get(i));
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).getImgObj(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g2.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getX(),
                    this.pieces.get(i).getY(), null);
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g2.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);

        if (this.mushroom.isAlive())
            g2.drawImage(this.mushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.mushroom.getX(), this.mushroom.getY(), null);
        else
            g2.drawImage(this.mushroom.deadImage(), this.mushroom.getX(), this.mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.turtle.isAlive())
            g2.drawImage(this.turtle.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.turtle.getX(), this.turtle.getY(), null);
        else
            g2.drawImage(this.turtle.deadImage(), this.turtle.getX(), this.turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
    }
}
